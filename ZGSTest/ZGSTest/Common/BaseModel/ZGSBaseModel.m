//
//  ZGSBaseModel.m
//  ZGSTest
//
//  Created by ZhiyongXu on 15/12/24.
//  Copyright © 2015年 ZhiyongXu. All rights reserved.
//

#import "ZGSBaseModel.h"
#import "NSString+getClassNameFromAttribute.h"
#import <objc/runtime.h>

@interface ZGSBaseModel () {
    NSDictionary *_customMapKeyDict;
    NSDictionary *_jsonDictionary;
}

@end

@implementation ZGSBaseModel

+ (NSSet *)getBaseModelPropertyKeys {
    NSMutableSet *keySet = [NSMutableSet set];
    
    unsigned int propertyCount = 0;
    
    objc_property_t *propertyList = class_copyPropertyList([self class], &propertyCount);
    
    for (int i = 0; i<propertyCount; i++) {
        objc_property_t property = propertyList[i];
        
        [keySet addObject:@(property_getName(property))];
        
        // NSLog(@"attribute is %@", @(property_getAttributes(property)));
    }
    return keySet;
}

+ (NSDictionary *)CustomMapKeyDict {
    return @{};
}

- (id)initWithDictionary:(NSDictionary *)dict {
    if (self = [super init]) {
        _jsonDictionary = dict;
        [self setValuesForKeysWithDictionary:dict];
    }
    
    return self;
}

+ (NSSet *)getMethodList {
    NSMutableSet *methodSet = [NSMutableSet set];
    
    unsigned int methodCount = 0;
    
    Method *methodList = class_copyMethodList([self class], &methodCount);
    
    for (int i = 0; i<methodCount; i++) {
        Method method = methodList[i];
        SEL sel = method_getName(method);
        NSLog(@"method name is %@", @(sel_getName(sel)));
        [methodSet addObject:@(sel_getName(sel))];
    }
    
    return methodSet;
}

- (id)modelFromJSONDictionary:(NSDictionary *)jsonDict {
    // id model = [[[self class] alloc] init];
    NSSet *propertySet = [[self class] getBaseModelPropertyKeys];
    NSDictionary *transformNameDict = [[self class] CustomMapKeyDict];
    for (NSString *propertyName in propertySet) {
        NSString *realName = nil;
        if (!transformNameDict[propertyName]) {
            realName = propertyName;
        }else
        {
            realName = transformNameDict[propertyName];
        }
        
        id value = jsonDict[realName];
        
        [self setValue:value forKey:propertyName];
        if ([value isKindOfClass:[NSDictionary class]]) {
            NSString *attribute = @(property_getAttributes(class_getProperty([self
                                                                              class], propertyName.UTF8String)));
            NSString *className = [attribute getClassNameFromAttribute];
            Class modelClass = NSClassFromString(className);
            if ([modelClass isSubclassOfClass:[ZGSBaseModel class]]) {
                
                id model = [[modelClass alloc] modelFromJSONDictionary:value];
                [self setValue:model forKey:propertyName];
            }
            
        }
    }
    return self;
}

- (void)setValue:(id)value forUndefinedKey:(NSString *)key {
    _customMapKeyDict = [[self class] CustomMapKeyDict];
    for (NSString *modelKey in _customMapKeyDict.allKeys) {
        
        if ([_customMapKeyDict[modelKey] isEqualToString:key]) {
            
            [self setValue:_jsonDictionary[key] forKey:modelKey];
        }
    }
    
}
@end
