//
//  ZGSBaseModel.h
//  ZGSTest
//
//  Created by ZhiyongXu on 15/12/24.
//  Copyright © 2015年 ZhiyongXu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZGSBaseModel : NSObject

+ (NSSet *)getBaseModelPropertyKeys;

+ (NSDictionary *)CustomMapKeyDict;

- (id)initWithDictionary:(NSDictionary *)dict;

+ (NSSet *)getMethodList;

- (id)modelFromJSONDictionary:(NSDictionary *)jsonDict;

@end
