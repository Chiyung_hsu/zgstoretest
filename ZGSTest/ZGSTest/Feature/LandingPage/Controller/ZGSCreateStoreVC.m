//
//  ZGSCreateStoreVC.m
//  ZGStore
//
//  Created by ZhiyongXu on 15/12/15.
//  Copyright © 2015年 ZhiyongXu. All rights reserved.
//

#import "ZGSCreateStoreVC.h"

@interface ZGSCreateStoreVC ()
@property (weak, nonatomic) IBOutlet UIScrollView *createStore;

@end

@implementation ZGSCreateStoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"创建一间店铺";
    self.createStore.contentSize = CGSizeMake(self.view.frame.size.width, 700);
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleDone target:nil action:nil];
}






@end
